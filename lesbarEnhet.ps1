foreach ($tall in $input) {
if ($tall -gt 1TB) { Write-Output "$($tall/1TB)TB"}
elseif ($tall -gt 1GB) { Write-Output "$($tall/1GB)GB"}
elseif ($tall -gt 1MB) { Write-Output "$($tall/1MB)MB"}
elseif   ($tall -gt 1KB) { Write-Output "$($tall/1KB)KB"}
else {Write-Output "$($tall)B" }
}

