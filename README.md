# Obligatorisk oppgave 3 - PowerShell pipelines og scripting

Denne oppgaven best�r av de f�lgende laboppgavene fra kompendiet:

* 11.6.c (Prosesser og tr�der)
* 12.4.c (En prosess sin bruk av virtuelt og fysisk minne)
* 13.10.b (Prosesser og tr�der)
* 13.10.c (Informasjon om deler av filsystemet)

SE OPPGAVETEKST I KOMPENDIET. HUSK � REDIGER TEKSTEN NEDENFOR!

## Gruppemedlemmer

Kenneth Tran 

## Kvalitetssikring

Jeg brukte den pipelinen som fulgte med denne obligen og det meste av kodene gikk gjennom. 
P� f�rste oppgave menynr 5, s� skulle man finne andel av Cpu-tiden som ble benyttet i kernelmode og i usermode siste sekund. Jeg fant bare engelske metoder til � komme seg til l�sningen. Det finnes sikkert en kode som vil v�re kj�rebar p� det norske systemet. 
