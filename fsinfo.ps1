$dir = $args[0]
if (Get-ChildItem $dir) {					
    $drive = (get-item $dir).psdrive
    $brukt = $drive.used
    $ledig = $drive.free
    $total = ($brukt + $ledig)
    $percent = (($brukt / $total)*100)
    $filene = (Get-ChildItem -Path $dir -Recurse -Attributes !directory)
    $storst = ($filene | sort-object length | Select-Object -last 1)
    $ant = $filene.count
    write-output "Partisjonen $dir befinner seg paa er $percent% full"
    write-output "Det finnes $ant filer."
    write-output "Den storste er $($storst.fullname) som er $($storst.length | .\lesbarEnhet.ps1) stor."
    write-output "Gjennomsnittlig filstorrelse er $($($filene | measure-object -Property length -Average).Average | .\lesbarEnhet.ps1)"
}

else {
    write-output "Tar inn kun et directory"
}