function VisMeny
{
	param(
		 [String]$Tittel = "Velkommen til menyen"
	)
	clear-Host 
	Write-Host "Velkommen til menyen"
Write-Output " 1 - Hvem er jeg og hva er navnet paa dette scriptet?"
Write-Output " 2 - Hvor lenge er det siden siste boott?"
Write-Output " 3 - Hvor mange prosesser og traader finnes?"
Write-Output " 4 - Hvor mange context switch'er fant sted siste sekund?"
Write-Output " 5 - Hvor stod andel av CPU-tiden ble benyttet i kernelmode og i usermode siste sekund?"
Write-Output " 6 - Hvor mange interrupts fant sted siste sekund?"
Write-Output " 9 - Avslutt dette scriptet"
}
function lastboot {
$osTemp = Get-WmiObject win32_operatingsystem 
$oppetid = (Get-Date) - ($osTemp.ConvertToDateTime($osTemp.lastbootuptime))
$utskrift = "Oppetid: " + $oppetid.Days + " dager, " + $oppetid.Hours + " timer, " + $oppetid.Minutes + " minutter" 
Write-Output "$utskrift"
}

function kernelBruker {
$cpuAndel = “\processor(_total)\% privileged time”,
			“\processor(_total)\% user time”
			Get-Counter -Counter $cpuAndel
}
function antInterrupts {
$antTemp = “\processor(_total)\interrupts/sec”
    Get-Counter -Counter $anTemp

}

do 
 { 
	VisMeny 
	$input = Read-Host -Prompt "Velg en funksjon"
	switch ($input)
	{   
			  '1' {
              Write-Output "Jeg er $env:username, navnet paa filen er $($MyInvocation.MyCommand.Name)"
			} '2' { 
			  lastboot
			} '3' { 
			  "Det er $((Get-Process).count) prosesser og det finnes $((get-ciminstance win32_thread).count) traader"
			} '4' {
			  # Paa engelsk er det, ((get-counter -Counter "\System\Context Switches/sec").Readings)
               Write-Output "Det er $((Get-Counter -Counter \System\Kontekstvekslinger/sek).readings)" 
			} '5' {
				kernelBruker			#Funker kun på engelsk systemspråk 
			} '6' {				
			   antInterrupts			#Funker kun på engelsk systemspråk 
			} '9' {
				return

     } 
	 }
	 pause
     
	}
	while($input -eq '9')

   


		  