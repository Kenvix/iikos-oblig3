foreach ($i in $args) {
	$file = "$i-$(get-date -UFormat "%Y%m%d-%H%M%S").meminfo"
	Write-Output "******** Minne info om prosess med PID $i ********" > $file
    Write-Output "Total bruk av virtuelt minne: $((Get-Process -Id $i).vm | .\lesbarEnhet.ps1)" >> $file
	Write-Output "Storrelse paa Working Set: $((Get-Process -Id $i).ws | .\lesBarEnhet.ps1) " >> $file
    Write-Output "Skrevet til fil for PID $i"
}